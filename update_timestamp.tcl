# Creates a constant in a verilog file with the specified hex value
proc generate_timestamp { value } {
    if { [catch {
        set fh [open "timestamp.v" w ]
        puts $fh "module timestamp (data_out);"
        puts $fh "output wire \[31:0\] data_out = 32'h${value};"
        puts $fh "endmodule"
        close $fh
    } res ] } {
        return -code error $res
    } else {
        return 1
    }
}

foreach { flow project revision } $quartus(args) { break }

#set str [clock format [clock seconds] -format {%y%m%d}]
#set revision [expr [clock seconds] >> 16]
set revision [format %x [clock seconds]]

if { [catch { generate_timestamp $revision } res] } {
    post_message -type critical_warning \
        "Couldn't generate Timestamp file. $res"
} else {
    post_message "updated timestamp to [clock format [expr 0x$revision]]"
}
