#! quartus timing analyzer custom report script
# run from timequest: source script/time_report.tcl

#puts "Clocks:"
#
#foreach_in_collection clk [all_clocks] {
#    puts [get_clock_info -name $clk]
#}

puts "Clock frequency:"

set domain_list [get_clock_fmax_info]

foreach domain $domain_list {
    set name [lindex $domain 0]
    set fmax [lindex $domain 1]
    set restricted_fmax [lindex $domain 2]
    set period [get_clock_info -period $name]
    set freq [ expr 1000/$period ]
    if {$freq < $restricted_fmax} {
        puts "Clock $name : Freq = $freq, Fmax = $fmax, Restricted Fmax = $restricted_fmax"
        post_message "Clock $name : Freq = $freq, Fmax = $fmax, Restricted Fmax = $restricted_fmax"
    } else {
        puts "Bad clock $name : Freq = $freq, Fmax = $fmax, Restricted Fmax = $restricted_fmax"
        post_message -type critical_warning "Bad clock $name : Freq = $freq, Fmax = $fmax, Restricted Fmax = $restricted_fmax"
    }
}

puts "Clock domains:"

foreach domain $domain_list {
    set name [lindex $domain 0]

    set path_count [ expr {0+0} ]
    set slack [ expr {0+0} ]
    foreach_in_collection path [ get_timing_paths -from_clock $name -to_clock $name -setup -npaths 1 ] {
        set path_count [ expr {$path_count+1} ]
        set slack [ get_path_info $path -slack]
    }
    #puts "Clock $name: number of paths: $path_count, slack: $slack"
    if {($path_count > 0) && ($slack < 90)} {
        if {$slack < 0} {
            post_message -type critical_warning "Bad slack $slack, clock $name"
        }
        puts "Slack $slack, Clock $name:"
        report_timing -from_clock $name -to_clock $name -setup -npaths 100 -panel_name "Slack $slack Clock $name"
    }
}

puts "Clock transfers:"

foreach domain $domain_list {
    set name [lindex $domain 0]

    foreach domain_to $domain_list {
        set name_to [lindex $domain_to 0]
        if {$name != $name_to} {
            #puts "Clock $name to $name_to:"
            set path_count [ expr {0+0} ]
            set slack [ expr {0+0} ]
            foreach_in_collection path [ get_timing_paths -from_clock $name -to_clock $name_to -setup -npaths 1 ] {
                set path_count [ expr {$path_count+1} ]
                set slack [ get_path_info $path -slack]
            }
            #puts "number of paths: $path_count, slack: $slack"
            if {($path_count > 0) && ($slack < 80)} {
                if {$slack < 0} {
                    post_message -type critical_warning "Bad slack $slack, clock transfer $name to $name_to"
                }
                puts "Slack $slack, Clock transfers $name to $name_to:"
                report_timing -from_clock $name -to_clock $name_to -setup -npaths 100 -panel_name "Clock transfers $slack $name to $name_to"
            }
        }
    }
}
        
#report_timing -setup -npaths 20 -detail full_path -multi_corner -panel_name "Custom report"

report_metastability -nchains 200000 -panel_name {Report Metastability} -file "output_files/metastability.rpt"

#end
