// Assumes 1-wait state read from status 
// Assumes 0-wait state write to control
// Reset should DEASSERT synchronously
module avalon_ctrl_slave (
	clk,
	rst,
	address,
	write,
	writedata,
	byteenable,
	read,
	readdata,
	readdatavalid,
	ctrl,
	ctrl_write
);

parameter NUM_CTRL_REGS = 0;
parameter SZ_WIDTH = 32;

localparam SZ_ADDR = $clog2(NUM_CTRL_REGS);

input wire clk;
input wire rst;
input wire [SZ_ADDR-1:0] address;
input wire write;
input wire [SZ_WIDTH-1:0] writedata;
input wire read;
input wire [3:0] byteenable;
output wire readdatavalid;
output wire [NUM_CTRL_REGS-1:0][SZ_WIDTH-1:0] ctrl;
output wire [NUM_CTRL_REGS-1:0] ctrl_write;
output wire [SZ_WIDTH-1:0] readdata;

wire [NUM_CTRL_REGS-1:0][SZ_WIDTH-1:0] status;

assign ctrl = status;

genvar j;
genvar n;
generate        
for(n=0; n<NUM_CTRL_REGS; n++) begin: gen_word
    wire [3:0] ena_byte;
   
    for(j=0; j<4; j++) begin: gen_byte

        wire enabled;
        
        always@(*) begin 
            enabled = (write & byteenable[j] & (address == n))? 1'b1 : 1'b0;
            ena_byte[j] = enabled;
        end 
    
        register #( 
            .SZ_DATA(SZ_WIDTH/4)
        ) control_regs ( 
            .clk	( clk ),
            .rst	( rst ),
            .ena	( enabled ),
            .d		( writedata[(j*8) +: 8] ),
            .q		( status[n][(j*8) +: 8] )
        );

    end // block: gen_byte

    always@(*) begin 
       ctrl_write[n] = |ena_byte;
    end // always@ begin
end
endgenerate 

register #(
	.SZ_DATA(SZ_WIDTH)
) output_reg ( 
	.clk	( clk ),
	.rst	( rst ),
	.ena	( 1'b1 ),
	.d		( status[address] ),
	.q		( readdata )
);

always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		readdatavalid <= 1'b0;
	end else begin 
		// Just always update the status register with the current address 
		// The avalon decoding logic higher up handles this properly for us
		
		// On a read request assert readdatavalid, so pipelining works correctly!
		if (read) 
			readdatavalid <= 1'b1;
		else 
			readdatavalid <= 1'b0;
				
	end 			
end 

endmodule

