// Assumes 1-wait state read from status 
// Reset should DEASSERT synchronously
module avalon_stat_slave (
	clk,
	rst,
	address,
	byteenable,
	read,
	readdata,
	readdatavalid,
	status
);

parameter NUM_STAT_REGS = 0;
parameter SZ_WIDTH = 32;

localparam SZ_ADDR = $clog2(NUM_STAT_REGS);

input wire clk;
input wire rst;

input wire [SZ_ADDR-1:0] address;
input wire read;
input wire [3:0] byteenable;
input wire [NUM_STAT_REGS-1:0][SZ_WIDTH-1:0] status;

output wire readdatavalid;
output wire [SZ_WIDTH-1:0] readdata;

reg [SZ_WIDTH-1:0] r_status;

assign readdata = r_status;

always@(posedge rst, posedge clk) begin 
	if(rst) begin 
		r_status <= {SZ_WIDTH{1'b0}};
		readdatavalid <= 1'b0;
	end else begin 
		// Just always update the status register with the current address 
		// The avalon decoding logic higher up handles this properly for us
		r_status <= status[address];
		
		// On a read request assert readdatavalid, so pipelining works correctly!
		if (read) 
			readdatavalid <= 1'b1;
		else 
			readdatavalid <= 1'b0;
	end 	
		
end 

endmodule
