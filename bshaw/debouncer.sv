module debouncer (
	clk,
	rst,
	d,
	q
);

parameter DEBOUNCE_TIME = 1;

input wire clk;
input wire rst;
input wire d;
output reg q;

wire changing;
wire d_delayed;

// We use the delayed input to give us time to see if a change has occurred
synchronizer #(
	.NUM_SYNC( 4 ),
	.SZ_DATA( 1 )
) lvds_sync ( 
	.clk ( clk ),
	.rst ( rst ),
	.d	  ( d ),
	.q	  ( d_delayed )
);

// Only update to d_delayed if the input is stable (ie. not changing)
always@(posedge rst, posedge clk) begin
	if(rst) begin
		q <= 1'b0;
	end else begin
		q <= (~changing) ? d_delayed : q;
	end
end

// Look for transition from low->high or high->low
transition_detector #(
	.TCOUNT( DEBOUNCE_TIME )
) tdet (
	.clk ( clk ),
	.d	  ( d ),
	.q	  ( changing )
);

endmodule

module debouncer_variable (
	clk,
	rst,
	db_time,
	d,
	q
);


parameter MAX_DEBOUNCE_TIME = 1;

localparam SZ_DEBOUNCE = $clog2(MAX_DEBOUNCE_TIME+1);

input wire clk;
input wire rst;
input wire [SZ_DEBOUNCE-1:0] db_time;
input wire d;
output reg q;

wire changing;
wire d_delayed;

// We use the delayed input to give us time to see if a change has occurred
synchronizer #(
	.NUM_SYNC( 4 ),
	.SZ_DATA( 1 )
) lvds_sync ( 
	.clk ( clk ),
	.rst ( rst ),
	.d	  ( d ),
	.q	  ( d_delayed )
);

// Only update to d_delayed if the input is stable (ie. not changing)
always@(posedge rst, posedge clk) begin
	if(rst) begin
		q <= 1'b0;
	end else begin
		q <= (~changing) ? d_delayed : q;
	end
end

// Look for transition from low->high or high->low
transition_detector_variable #(
	.MAX_OFF_DELAY( MAX_DEBOUNCE_TIME )
) tdet (
	.clk ( clk ),
	.off_delay ( db_time ),
	.d	  ( d ),
	.q	  ( changing )
);

endmodule
