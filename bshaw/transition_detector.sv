/*
	Takes an input and tests it for transitions every clock cycle. 
	If the input changes from value the previous clock cycle, the output goes high. 
	If the input does not change for TCOUNT clock cycles, the output goes low. 
	The output will return high on the first transition.

*/
module transition_detector (
	clk,
	d,
	q
);

parameter TCOUNT = 1;

localparam SZ_TCOUNT = $clog2(TCOUNT+1);

input wire clk;
input wire d;
output reg q; 

reg prev_val;
reg [SZ_TCOUNT-1:0] count;

always@(posedge clk) begin
	prev_val <= d;
end

always@(posedge clk) begin
	if(d != prev_val) begin
		count <= {SZ_TCOUNT{1'b0}};
		q <= 1'b1;
	end else begin
		if(count < TCOUNT[SZ_TCOUNT-1:0]) begin
			count <= count + 1'b1;
			q <= 1'b1;
		end else begin
			count <= count;
			q <= 1'b0;
		end
	end
end
	
endmodule

module transition_detector_variable (
	clk,
	off_delay,
	d,
	q
);

parameter MAX_OFF_DELAY = 1;

localparam SZ_TCOUNT = $clog2(MAX_OFF_DELAY+1);

input wire clk;
input wire [SZ_TCOUNT-1:0] off_delay;
input wire d;
output reg q; 

reg prev_val;
reg [SZ_TCOUNT-1:0] count;

always@(posedge clk) begin
	prev_val <= d;
end

always@(posedge clk) begin
	if(d != prev_val) begin
		count <= {SZ_TCOUNT{1'b0}};
		q <= 1'b1;
	end else begin
		if(count < off_delay) begin
			count <= count + 1'b1;
			q <= 1'b1;
		end else begin
			count <= count;
			q <= 1'b0;
		end
	end
end
	
endmodule
