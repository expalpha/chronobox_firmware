#!/usr/bin/perl -w

$| = 1;

my $f = shift @ARGV;
die "Cannot read $f: $!\n" unless -r $f;

#my $quartus = "/home/quartus/quartus7.2";
#my $quartus = "/home/olchansk/altera7.2/quartus7.2";
#my $quartus = "/triumfcs/trshare/olchansk/altera/altera9.1/quartus";
#my $quartus = "/daq/daqshare/olchansk/altera/15.1/quartus";
my $quartus = "/opt/intelFPGA/17.0/quartus";

my $target = "5CSEBA6";

my $q = `$quartus/bin/jtagconfig`;
print $q;

die "Cannot find $target in available JTAG chains:\n $q" unless $q =~ /$target/;

@q = split(/\n\n/, $q);

my $b;

foreach my $qq (sort @q) {
  my ($bb) = $qq =~ /(\d)\)/;
  print "$bb XXX $qq\n";
  if ($qq =~ /$target/) {
    $b = $bb;
    last;
  }
}

die "Cannot find $target in available JTAG chains" unless $b > 0;

#die "Here $b!";
#
#
#$q =~ /1\) (.*)\n/m;
#my $b = $1;
#print "$b\n";

#my $b = "USB-Blaster [3-2.3]";
#my $b = "1";

#my $cmd = "$quartus/bin/quartus_pgm -c \"$b\" grifc.cdf";
my $cmd = "$quartus/bin/quartus_pgm -c \"$b\" -m JTAG -o \"p;$f\@2\"";
print "Running $cmd\n";
system $cmd;

exit 0;
#end
