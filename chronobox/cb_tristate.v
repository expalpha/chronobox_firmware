//
// special tristate
//

`default_nettype none

module cb_tristate #(parameter N = 32)
  (
   inout wire [N-1:0] pins,
   input wire [N-1:0] output_enable,
   input wire [N-1:0] outputs,
   output wire [N-1:0]  inputs
   );

   genvar               i;

   generate
      for (i=0; i<N; i=i+1) begin: altiobuf_loop
         altiobuf altiobuf_inst
           (
	    .datain(outputs[i]),
	    .oe(output_enable[i]),
	    .dataio(pins[i]),
	    .dataout(inputs[i])
	    );
      end
   endgenerate
         
   //assign inputs = pins;
   //assign pins = output_enable ? outputs : {(N){1'bz}};

endmodule
