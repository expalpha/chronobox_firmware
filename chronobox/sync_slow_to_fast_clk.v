`default_nettype none
module sync_slow_to_fast_clk
  (
   input wire in,
   input wire clk,
   output reg out
   );

  reg in1;

  always @(posedge clk) begin
     in1 <= in;
     out <= in1;
  end
endmodule
