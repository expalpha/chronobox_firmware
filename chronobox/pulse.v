module pulse
  (
   input wire clk,
   input wire in,
   output wire out
   );
   
   reg        in1;
   
   always @(posedge clk) begin
      in1 <= in;
   end

   assign out = in & !in1;
   
endmodule
