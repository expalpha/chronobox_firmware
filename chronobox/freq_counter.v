`default_nettype none
module frequency_counter
  (
   input wire 	      clk1,
   input wire 	      clk2,

   output wire [31:0] clk1_counter_out,
   output wire [31:0] clk2_counter_out
   );

   reg [31:0] 	      counter;
   reg 		      reset;
   reg 		      start1;
   reg 		      stop;

   always_ff @ (posedge clk1) begin
      if (counter == 0) begin
	 //counter <= 50;
	 counter <= 32'h10000000;
	 reset <= 0;
	 start1 <= 1;
	 stop <= 0;
      end else if (counter == 5) begin
	 counter <= counter - 1;
	 reset <= 1;
	 start1 <= 0;
	 stop <= 0;
      //end else if (counter == 30) begin
      end else if (counter == 32'h08000000) begin
	 counter <= counter - 1;
	 reset <= 0;
	 start1 <= 0;
	 stop <= 1;
      end else begin
	 counter <= counter - 1;
	 reset <= 0;
	 stop <= 0;
      end
   end
   
   reg [31:0] 	      clk1_counter;
   reg 		      start1x;

   always_ff @ (posedge clk1 or posedge reset) begin
      if (reset) begin
	 clk1_counter <= 0;
      end else begin
	 start1x <= start1;
	 if (start1==1 && start1x==0) begin
	    clk1_counter <= 0;
	 end else if (start1==1 && start1x==1) begin
	    clk1_counter <= clk1_counter + 1;
	 end else if (start1==0 && start1x==1) begin
	    clk1_counter_out <= clk1_counter;
	 end else begin
	 end
      end
   end

   reg 		      start2a;
   reg 		      start2b;
   reg 		      start2;

   always_ff @ (posedge clk2) begin
      start2a <= start1;
      start2b <= start2a;
      start2 <= start2b;
   end

   reg [31:0] 	      clk2_counter;
   reg 		      start2x;
   reg 		      clk2_is_running;
   reg [31:0] 	      clk2_counter_tmp;
   reg 		      clk2_counter_write;

   always_ff @ (posedge clk2 or posedge reset) begin
      if (reset) begin
	 clk2_counter <= 0;
	 clk2_is_running <= 0;
	 clk2_counter_tmp <= 0;
	 clk2_counter_write <= 0;
      end else begin
	 start2x <= start2;
	 clk2_is_running <= 1;
	 if (start2==1 && start2x==0) begin
	    clk2_counter <= 0;
	    clk2_counter_write <= 0;
	 end else if (start2==1 && start2x==1) begin
	    clk2_counter <= clk2_counter + 1;
	    clk2_counter_write <= 0;
	 end else if (start2==0 && start2x==1) begin
	    clk2_counter_tmp <= clk2_counter;
	    clk2_counter_write <= 1;
	 end else begin
	    clk2_counter_write <= 0;
	 end
      end
   end

   reg 		      ru1;
   reg 		      ru2;
   reg 		      wr1;
   reg 		      wr2;

   always_ff @ (posedge clk1) begin
      wr1 <= clk2_counter_write;
      wr2 <= wr1;
      ru1 <= clk2_is_running;
      ru2 <= ru1;
   end

   wire ru = ru2;
   wire wr = wr2;

   always_ff @ (posedge clk1) begin
      if (stop && !ru) begin
	 clk2_counter_out <= 0;
      end else if (wr) begin
	 clk2_counter_out <= clk2_counter_tmp;
      end
   end

endmodule
