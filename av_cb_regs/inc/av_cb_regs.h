#ifndef __AV_CB_REGS_H__
#define __AV_CB_REGS_H__

// Control Registers
#define AG_ADC16_THRESHOLD		0
#define AG_ADC32_THRESHOLD		1
#define AG_DAC_DATA		2
#define AG_DAC_CTRL		3

// Status Registers
#define AG_ADC16_BITS				0
#define AG_ADC32_BITS				1

#endif /* __AV_CB_REGS_H__ */
