#! Makefile
#
# Before using the Makefile, run:
#
# ### /opt/intelFPGA/20.1/nios2eds/nios2_command_shell.sh
# /daq/quartus/21.1/nios2eds/nios2_command_shell.sh
#

#SCRIPTDIR=$(dirname "$(readlink -f "$0")")
#PROJECT_PATH=$SCRIPTDIR/../hdl
#PROJECT_NAME="feam"
#PROJECT_REV="rev1"

QUARTUS_JTAG := /daq/quartus/13.0sp1/quartus/bin/jtagconfig
QUARTUS_PGM  := /daq/quartus/13.0sp1/quartus/bin/quartus_pgm

PROJECT_NAME := DE10_NANO_SoC_GHRD

#JTAG := -c \"$b\"
JTAG := 

all:: quartus

qsys:
	/usr/bin/time qsys-generate soc_system.qsys --synthesis=VERILOG $(QSYS_GENERATE_ARGS)

quartus:
	/usr/bin/time quartus_sh --flow compile $(PROJECT_NAME)
	-grep "Missing location assignment" output_files/$(PROJECT_NAME).fit.rpt
	-grep -i critical output_files/$(PROJECT_NAME).sta.rpt

jic:
	quartus_cpf -c $(PROJECT_NAME).cof

load_sof:
	$(QUARTUS_JTAG)
	$(QUARTUS_PGM) $(JTAG) -m JTAG -o "p;output_files/$(PROJECT_NAME).sof@2"

load_jic:
	$(QUARTUS_PGM) $(JTAG) -m JTAG -o "p;output_files/$(PROJECT_NAME).jic@2"

verify_jic:
	$(QUARTUS_PGM) $(JTAG) -m JTAG -o "v;output_files/$(PROJECT_NAME).jic@2"

# this does not work "Programming option E is illegal"
#read:
#	$(QUARTUS_PGM) $(JTAG) -m JTAG -o "e;read.bin;5CGXBC7B6"

clean:
	quartus_sh --clean $(PROJECT_NAME)

#end
