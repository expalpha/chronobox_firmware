#**************************************************************
# This .sdc file is created by Terasic Tool.
# Users are recommended to modify this file to match users logic.
#**************************************************************

#**************************************************************
# Create Clock
#**************************************************************
create_clock -period "50.0 MHz" [get_ports FPGA_CLK1_50]
create_clock -period "50.0 MHz" [get_ports FPGA_CLK2_50]
create_clock -period "50.0 MHz" [get_ports FPGA_CLK3_50]

# for enhancing USB BlasterII to be reliable, 25MHz
create_clock -name {altera_reserved_tck} -period 40 {altera_reserved_tck}
set_input_delay -clock altera_reserved_tck -clock_fall 3 [get_ports altera_reserved_tdi]
set_input_delay -clock altera_reserved_tck -clock_fall 3 [get_ports altera_reserved_tms]
set_output_delay -clock altera_reserved_tck 3 [get_ports altera_reserved_tdo]

create_clock -name GPIO_1_0 -period "10.0 MHz" [get_ports GPIO_1_0]

#**************************************************************
# Create Generated Clock
#**************************************************************
#derive_pll_clocks

create_generated_clock -source {pll_inst|pll_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL|refclkin} -divide_by 2 -multiply_by 12 -duty_cycle 50.00 -name pll_main {pll_inst|pll_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL|vcoph[0]}
create_generated_clock -source {pll_inst|pll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|vco0ph[0]} -divide_by 3 -duty_cycle 50.00 -name clk_100 {pll_inst|pll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}
create_generated_clock -source {pll_inst|pll_inst|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|vco0ph[0]} -divide_by 20 -duty_cycle 50.00 -name clk_15 {pll_inst|pll_inst|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|divclk}
create_generated_clock -source {pll_inst|pll_inst|altera_pll_i|general[2].gpll~PLL_OUTPUT_COUNTER|vco0ph[0]} -divide_by 30 -duty_cycle 50.00 -name clk_10 {pll_inst|pll_inst|altera_pll_i|general[2].gpll~PLL_OUTPUT_COUNTER|divclk}

create_generated_clock -master_clock GPIO_1_0 -source GPIO_1_0 -multiply_by 30 -duty_cycle 50.00 -name pll_ext {ts_clk_pll|ts_clk_pll_inst|altera_pll_i|cyclonev_pll|fpll_0|fpll|vcoph[0]}
create_generated_clock -source {ts_clk_pll|ts_clk_pll_inst|altera_pll_i|cyclonev_pll|counter[0].output_counter|vco0ph[0]} -divide_by 30 -duty_cycle 50.00 -name clk_ts {ts_clk_pll|ts_clk_pll_inst|altera_pll_i|cyclonev_pll|counter[0].output_counter|divclk}

#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************
derive_clock_uncertainty



#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************



#**************************************************************
# Set Load
#**************************************************************

#
# clock setup from yair
#

proc puts_and_post_message { x } {
    # set module [lindex $quartus(args) 0]                                                                                                                
    # if [string match "quartus_map" $module] {                                                                                                           
    # } else {                                                                                                                                            
    if { ([info commands post_message] eq "post_message") || ([info procs post_message] eq "post_message") } {
        puts $x
        post_message $x
    }
    # }                                                                                                                                                   
}

proc print_collection { col } {
    if {[catch {
        foreach_in_collection c $col  {
            puts_and_post_message "   [get_clock_info -name $c]";
        }
    } ] } {
        if {[catch {
            puts_and_post_message "[query_collection $col -all -report_format]"
        } ] } {
            puts_and_post_message "   $col"
        }
    }
}

proc timid_asynchronous { clock_name clockB { max_delay  50.0 } { min_delay  -50 } } {
    set_max_delay -from  $clock_name -to $clockB $max_delay
    set_max_delay -from $clockB -to  $clock_name $max_delay
    set_min_delay -from  $clock_name -to $clockB $min_delay
    set_min_delay -from $clockB -to  $clock_name $min_delay
    #puts_and_post_message "=========================================================================="
    #puts_and_post_message "timid_asynchronous $clock_name from/to $clockB, max_delay = $max_delay min_delay = $min_delay which means:"
    #print_collection $clock_name
    #puts_and_post_message "from/to: "
    #print_collection $clockB
    #puts_and_post_message "=========================================================================="
}

proc timid_asynchronous_group { clock_list { max_delay  50.0 } { min_delay  -50 } } {
    puts [concat "timid_asynchronous_group: " [llength $clock_list] ":" [join $clock_list]]
    for {set i 0 } { $i < [llength $clock_list] } { incr i } {
        for {set j [expr $i + 1] } { $j < [llength $clock_list] } { incr j }  {
            puts [concat "timid_asynchronous clocks " [lindex $clock_list $i] " and " [lindex $clock_list $j]" ]
            timid_asynchronous [lindex $clock_list $i] [lindex $clock_list $j] $max_delay $min_delay;
        }
    }
}

timid_asynchronous_group {
  clk_10
  clk_15
  clk_100
  clk_ts
  GPIO_1_0
}

#
# Soc signals
#

set_false_path -from * -to [get_ports *HPS_ENET*]
set_false_path -from [get_ports *HPS_ENET*] -to *

set_false_path -from * -to [get_ports *HPS_USB*]
set_false_path -from [get_ports *HPS_USB*] -to *

set_false_path -from * -to [get_ports *HPS_SD*]
set_false_path -from [get_ports *HPS_SD*] -to *

set_false_path -from * -to [get_ports *HPS_UART*]
set_false_path -from [get_ports *HPS_UART*] -to *

set_false_path -from * -to [get_ports *HPS_I2C*]
set_false_path -from [get_ports *HPS_I2C*] -to *

set_false_path -from * -to [get_ports *HPS_SPIM*]
set_false_path -from [get_ports *HPS_SPIM*] -to *

create_clock -name HPS_I2C0_SCLK -period "50.0 MHz" [get_ports HPS_I2C0_SCLK]
create_clock -name HPS_I2C1_SCLK -period "50.0 MHz" [get_ports HPS_I2C1_SCLK]
create_clock -name HPS_USB_CLKOUT -period "50.0 MHz" [get_ports HPS_USB_CLKOUT]

#
# DE10-Nano signals
#

set_false_path -from * -to [get_ports KEY*]
set_false_path -from [get_ports KEY*] -to *

set_false_path -from * -to [get_ports SW*]
set_false_path -from [get_ports SW*] -to *

set_false_path -from * -to [get_ports LED*]
set_false_path -from [get_ports LED*] -to *

#
# Chronobox signals
#

set_false_path -from * -to [get_ports *GPIO_0_*]
set_false_path -from [get_ports *GPIO_0_*] -to *

set_false_path -from * -to [get_ports *GPIO_1_*]
set_false_path -from [get_ports *GPIO_1_*] -to *

set_false_path -from * -to {sld_signaltap:*}

set_false_path -from * -to {FlashProgrammer:*}
set_false_path -to * -from {FlashProgrammer:*}

set_false_path -from {*|reconfig_out} -to *
set_false_path -from {cb_regs:cb_regs|reconfig_out} -to *

#set_false_path -from * -to          {cb_main:cb_main|inputs1[*]}
#set_false_path -from {GPIO_1_0} -to {cb_main:cb_main|inputs1[58]}
#set_false_path -from {GPIO_1_0} -to *

#set_false_path -from * -to [get_pins {cb_main|inputs1[*]}]

# cut clock transfer paths reported by timequest report on "clocks transfer"

set_false_path -from {frequency_counter:fc_ext|start1} -to {frequency_counter:fc_ext|start2a}
set_false_path -from {frequency_counter:fc_ext|reset}  -to {frequency_counter:fc_ext|start2x}
set_false_path -from {frequency_counter:fc_ext|clk2_counter_write} -to {frequency_counter:fc_ext|wr1}
set_false_path -from {frequency_counter:fc_ext|clk2_is_running} -to {frequency_counter:fc_ext|ru1}
set_false_path -from {frequency_counter:fc_ext|clk2_counter_tmp[*]} -to {frequency_counter:fc_ext|clk2_counter_out[*]}

set_false_path -from {frequency_counter:fc_ext|start1} -to {frequency_counter:fc_ts|start2a}
set_false_path -from {frequency_counter:fc_ext|reset}  -to {frequency_counter:fc_ts|start2x}
set_false_path -from {frequency_counter:fc_ts|clk2_counter_write} -to {frequency_counter:fc_ts|wr1}
set_false_path -from {frequency_counter:fc_ts|clk2_is_running} -to {frequency_counter:fc_ts|ru1}
set_false_path -from {frequency_counter:fc_ts|clk2_counter_tmp[*]} -to {frequency_counter:fc_ts|clk2_counter_out[*]}

set_false_path -from {cb_regs:cb_regs|reg18_out[*]}
set_false_path -from {cb_regs:cb_regs|reg19_out[*]}
set_false_path -from {cb_regs:cb_regs|reg20_out[*]}
set_false_path -from {cb_regs:cb_regs|reg21_out[*]}
set_false_path -from {cb_regs:cb_regs|reg22_out[*]}
set_false_path -from {cb_regs:cb_regs|reg23_out[*]}

set_false_path -from {cb_regs:cb_regs|reg30_cb_sync_a_out[*]}
set_false_path -from {cb_regs:cb_regs|reg31_cb_sync_b_out[*]}

set_false_path -to {cb_main:cb_main|sync_in_clk_ts1}
set_false_path -to {cb_main:cb_main|sync_arm_clk_ts1}
set_false_path -to {cb_main:cb_main|sync_disarm_clk_ts1}

set_false_path -from {cb_main:cb_main|ts_overflow_data[*]} -to {cb_main:cb_main|tso_data[*]}
set_false_path -to {cb_main:cb_main|sync_slow_to_fast_clk:tso_sync_inst|in1}

set_false_path -from {cb_main:cb_main|sync_armed_clk_ts}    -to {cb_main:cb_main|sync_status_out[*]}
set_false_path -from {cb_main:cb_main|sync_received_clk_ts} -to {cb_main:cb_main|sync_status_out[*]}
set_false_path -from {cb_main:cb_main|sync_done_clk_ts}     -to {cb_main:cb_main|sync_status_out[*]}

#end
