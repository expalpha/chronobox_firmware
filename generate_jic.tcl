set module [lindex $quartus(args) 0]
set project [lindex $quartus(args) 1] 
set revision [lindex $quartus(args) 2] 

if [string match "quartus_asm" $module] {
    # Include commands here that are run after the assember
    post_message "Generating files from $project.cof"
    
    # If the command can't be run, return an error.
    set status [catch {exec quartus_cpf -c $project.cof} output option]
    if {$status == 0} {
        post_message "Successfully ran $project.cof"
    } else {		
        set err_info [lassign [dict get $option -errorcode] err_type]
        switch -exact -- $err_type {
            default {
                post_message -type error "Error occurred running $project.cof\nError: $output"	
            }
        }
    }
}
