`default_nettype none
module av_cb_regs
  #(
    parameter SZ_DATA = 32,
    parameter SZ_ADDR = 14
    )
   (
    // Avalon Interface
    input wire clk, 
    input wire rst, 
    
    input  wire [SZ_ADDR-1:0] s1_address, 
    input  wire               s1_read,
    output wire [SZ_DATA-1:0] s1_readdata, 
    output wire               s1_readdatavalid, 
    input  wire [3:0]         s1_byteenable,
    input  wire               s1_write, 
    input  wire [SZ_DATA-1:0] s1_writedata,
    
    output wire [SZ_ADDR-1:0] addr_out,
    output wire [SZ_DATA-1:0] write_data_out,
    output wire               write_strobe_out,
    output wire               read_strobe_out,
    input  wire [SZ_DATA-1:0] read_data,
    input  wire               read_data_valid
    );
   
   assign  addr_out           = s1_address;
   assign  write_data_out     = s1_writedata;
   assign  write_strobe_out   = s1_write;
   assign  read_strobe_out    = s1_read;
   assign  s1_readdata        = read_data;
   assign  s1_readdatavalid   = read_data_valid;

endmodule
