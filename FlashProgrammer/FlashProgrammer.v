module FlashProgrammer
  (
   input wire [15:0]  in,
   output wire [15:0] out
   );
   
wire	gnd = 0;

wire	[7:0] hexAB = 8'hAB;
wire	[7:0] hexCD = 8'hCD;

wire	[3:0] asmi_asd;
wire	[3:0] asmi_data;
wire	asmi_dclk = in[0];
wire	asmi_ncs = in[2];
wire    asmi_oe = (in[15:8] == hexAB);
wire	asmi_noe =  ~asmi_oe;
wire	[7:0] oe_flag = hexCD & {asmi_oe,asmi_oe,asmi_oe,asmi_oe,asmi_oe,asmi_oe,asmi_oe,asmi_oe};

assign	out = {gnd,gnd,gnd,asmi_data[1],gnd,gnd,gnd,gnd,oe_flag[7:0]};

assign	asmi_asd = {in[6],in[6],in[6],in[6]};

wire req;
wire grant = asmi_noe & req;

serial_flash_loader sfl
(
 .noe_in              (asmi_noe),              //              noe_in.noe
 .dclk_in             (asmi_dclk),             //             dclk_in.dclkin
 .ncso_in             (asmi_ncs),             //             ncso_in.scein
 .data_in             (asmi_asd),             //             data_in.data_in
 .data_oe             (asmi_oe),             //             data_oe.data_oe
 .asmi_access_granted (grant), // asmi_access_granted.asmi_access_granted
 .data_out            (asmi_data),            //            data_out.data_out
 .asmi_access_request (req)  // asmi_access_request.asmi_access_request
);

endmodule
